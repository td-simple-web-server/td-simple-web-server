#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/sendfile.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>

#include "net.h"
#include "event.h"

void setnonblocking(int fd)
{
        int opts;
        if ((opts = fcntl(fd, F_GETFL)) < 0) {
                fprintf(stderr, "fcntl failed\n");
                return;
        }

        opts = opts | O_NONBLOCK;

        if (fcntl(fd, F_SETFL, opts) < 0) {
                fprintf(stderr, "fcntl failed\n");
                return;
        }

        return;
}

int net_listen(int port)
{
        int sfd, opt = 1;
        struct sockaddr_in sin;

        if ((sfd = socket(AF_INET, SOCK_STREAM, 0)) <= 0) {
                perror("Socket failed\n");
                exit (-1);
        }

        setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (const void*)&opt, sizeof(opt));

        memset(&sin, 0, sizeof(struct sockaddr_in));

        sin.sin_family = AF_INET;
        sin.sin_port = htons((short)((port ? port : PORT)));
        sin.sin_addr.s_addr = INADDR_ANY;

        if (bind(sfd, (struct sockaddr *)&sin, sizeof(sin)) != 0) {
                perror("bind failed\n");
                exit (-1);
        }

        if (listen(sfd, 1024) != 0) {
                perror("listen failed\n");
                exit (-1);
        }

        return sfd;
}

/* Send file using system call: sendfile
 * Return 0 is send ok, 1 is buffer full, send again, continue wait event
 */
int net_sendfile(_epoll_data *ed)
{
        int fd, n = 0, err = 0;

        int no = 1;
        int to_send;
        size_t r;

        if (ed->fd == -1)
                return -1;

        to_send = ed->fsize - ed->fpos;
        if (to_send <= 0)
                return 0;

        setsockopt(ed->fd, IPPROTO_TCP, TCP_CORK, (char*)&no, sizeof(int));

        if (ed->ofd == -1 && (ed->ofd = open(ed->filename, O_RDONLY)) == -1) {
                fprintf(stderr, "Can not open file: %s\n", ed->filename);
                return -1;
        }

        //printf("Count is : %d, offset is :%d, %d, %d\n", ed->fsize, ed->fpos, to_send + ed->fpos, to_send);

#ifdef HAVE_POSIX_FADVISE
        /* tell the kernel that we want to stream the file */
        if (posix_fadvise(ed->ofd, 0, 0, POSIX_FADV_SEQUENTIAL) == -1) {
                if (ENOSYS != errno) {
/*                        log_error_write(srv, __FILE__, __LINE__, "ssd",
                                        "posix_fadvise failed:",
                                        strerror(errno), c->file.fd);
                                        */
                }
        }
#endif

        if ((r = sendfile(ed->fd, ed->ofd, (off_t*)&ed->fpos, to_send)) == -1) {
                switch (errno) {
                        /* send later */
                        case EAGAIN:
                        case EINTR:
                                break;

                        case EPIPE:
                        case ECONNRESET:
                                printf("Send error:%d \n", errno);
                                err = -1;
                                break;

                        default:
                                printf("Send error:%d \n", errno);
                                err = -2;
                }

		destroy_event(ed);
		/*
                close(ed->ofd);
		close(ed->fd);
                del_event(ed);
                if (ed->hhd)
                        free(ed->hhd);
                free(ed);
		*/
//                printf("free error fpos\n");
                return(err);
        }

	/* Send completed */
        if (ed->fpos == ed->fsize || r + ed->fpos == ed->fsize) {
		destroy_event(ed);
		/*
                del_event(ed);
                close(ed->ofd);
                close(ed->fd);
                if (ed->hhd)
                        free(ed->hhd);
                free(ed);
		*/
                return 0;
        }

        add_out_event(ed);
        
        return 0;
}
