#ifndef HTTP_INCLUDED
#define HTTP_INCLUDED

#include "event.h"
#include "lib_http.h"

#define TD_NAME                 "TD"

#define MIN(a,b)                (a>b?b:a)
#define MAX(a,b)                (a<b?b:a)

#define HTTP_HEADER_SIZE        (1024*128)
#define HTTP_LINE_SIZE          512
#define HTTP_HEADER_LINES       20

#define HTTP_ERR_FILE_DIR       "err/"

#define HTTP_HEADER_200         "HTTP/1.1 200 OK"
#define HTTP_HEADER_404         "HTTP/1.1 404 Not Found"
#define HTTP_HEADER_403         "HTTP/1.1 403 Forbidden"
#define HTTP_HEADER_400         "HTTP/1.1 400 Bad Request"

#define HTTP_MSG_404            "<html><title>404 not found</title><body><h1>404 Page Not Found.</h1></body></html>"
#define HTTP_MSG_403            "<html><title>403 Forbidden</title><body><h1>403 Forbidden.</h1></body></html>"
#define HTTP_MSG_400            "<h1>400 Bad request</h1>"
#define HTTP_MSG_DEFAULT        "<h1>Request error</h1>"

#define ERR_HTTP_BAD_HEADER     -1

#define HTTP_PARAM_SIZE         512
#define HTTP_MAX_POST_SIZE      (2*1024*1024)


typedef struct {
        char method[HTTP_PARAM_SIZE]; /* http method */
        char agent[HTTP_PARAM_SIZE]; /* user agent */
        char path[HTTP_PARAM_SIZE];  /* request path */
        char ppath[HTTP_PARAM_SIZE];
        char qs[HTTP_PARAM_SIZE];    /* qeury string */
        char file[HTTP_PARAM_SIZE];
        char ext[12];
        char upath[HTTP_PARAM_SIZE];
        char mimetype[HTTP_PARAM_SIZE];
        char host[HTTP_PARAM_SIZE];  /* host */
        char accept[HTTP_PARAM_SIZE]; /* accept */
        char connection[HTTP_PARAM_SIZE]; /* accept */
        char proto[HTTP_PARAM_SIZE]; /* accept */
        char content_type[HTTP_PARAM_SIZE]; /* content type */
        char post[HTTP_MAX_POST_SIZE];
} _http_header_data;

http_req_headers *parse_header(char *buf);
int http_proto(_epoll_data *epoll_data);
int http_header(int fd, int code, char *content_type,
                char *charset, int length);

#endif
