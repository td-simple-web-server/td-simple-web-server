#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include "base.h"
#include "mime.h"
#include "str.h"
#include "hashtab.h"

struct hashtab *init_mime_set()
{
        FILE *fp;
        char line[512];
        char mime[MIME_DATA_SIZE];
        char key[MIME_KEY_SIZE];
        char *saveptr;
	char *t = NULL;
        int size, id = 0;
	int rc;
        int i;
        int first = 0, c = 0, end = 0, j = 0, tsize = 0, rksize = 0, rmsize = 0;
	char *f;

	struct hashtab *mime_set;

	if (!(mime_set = init_elfhashtab(MIME_SET_SIZE))) {
                printf("MIME hash create error\n");
                exit(-1);
        }

        if ((fp = fopen(MIME_TYPE_FILE, "r")) == NULL) {
                printf("Could not read Mime type file: %s\n", MIME_TYPE_FILE);
                exit (-1);
        }

        while (fgets(line, 511, fp)) {

                first   = 0;
                size    = strlen(line);

                if (line[0] == '#' || line[0] == '\n' || size == 1)
                        continue;

                memset(key, 0, MIME_KEY_SIZE);
                memset(mime, 0, MIME_DATA_SIZE);
		f 	= NULL;
		tsize	= 0;
		rksize	= 0;
		rmsize	= 0;

                for (i = 0; i < size; i++) {

                        c 	= 0;
			end 	= 0;
			tsize	= 0;

                        if (!isblank(line[i]))
                                c = 1;

                        if (i > 0 && !first && (isblank(line[i]))) {
				memset(mime, 0, MIME_DATA_SIZE);
				rmsize = MIN((MIME_DATA_SIZE-1),i);
                                strncpy(mime, line, rmsize);
				//printf("Line: %d, %d\n", rmsize, strlen(mime));
			//	printf("Mime: %s\n", mime);
                                first = 1;
                                continue;
                        }

			if (first && c && f == NULL) {
				memset(key, 0, MIME_KEY_SIZE);
				f = &line[i];
				continue;
			}

                        if (first && (!c || line[i] == '\n') && tsize == 0 && f != NULL) {
				tsize = &line[i] - f;
				rksize = MIN((MIME_KEY_SIZE-1),tsize);
				strncpy(key, f, rksize);
				tsize = 0;
				f = NULL;
			//	printf("mime.c :--%s--,%sEND\n", key, mime);
				if ((rc = hashtab_insert(mime_set,
							 mkstr(key, rksize + 1),
							 mkstr(mime, rmsize + 1))))
					printf("Could not insert mime node:%d\n", rc);
			}
		}
	}
                        
        fclose(fp);
        return mime_set;
}

char *get_mimetype(struct hashtab *mime_set, char *key)
{
	strtolower(key);
	return hashtab_search(mime_set, key);
}
