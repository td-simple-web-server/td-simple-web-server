#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <time.h>

#include "http.h"
#include "lib_http.h"
#include "event.h"
#include "net.h"
#include "log.h"
#include "config.h"
#include "mime.h"
#include "dynscript.h"

extern char *root_dir;
extern global_config *_gcon;

http_req_headers *parse_header(char *buf)
{
        int i, size, j, b, t;
	int content_len = 0;
        char line[HTTP_LINE_SIZE];
	char len[12];
	http_req_headers *hrh;

	if (!(hrh = http_parse_header(buf)))
		return NULL;

        printf("Post: %s, Method: %d, Host: %s, Version: %s\n", 
	       hrh->post, hrh->method, hrh->host,
               hrh->version);

	return hrh;
	
	/*
        if ((hhd = malloc(sizeof(*hhd))) == NULL) {
                perror("Can not alloc http header_data\n");
                return NULL;
        }

        char *k;

        size = strlen(buf);
        b = t = j = 0;
        memset(line, 0, HTTP_LINE_SIZE);

        for (i = 0; i < size; i++) {
                if (buf[i] < 32 && buf[i] > 126)
                        continue;

                if (i < (size - 1) && buf[i] == '\r' && buf[i + 1] == '\n') {
                        ++b;
                        continue;
                }

                if (b == 1) {
			//printf("%d, %s\n", content_len, line);
			b = 0; j = 0;

                        if ((k = strstr(line, "GET")) != NULL ||
                            (k = strstr(line, "POST")) != NULL) {
                                sscanf(k, "%s %s %s",
                                       hhd->method, hhd->path, hhd->proto);
                                parse_uri(hhd);
                        } else if ((k = strstr(line, "User-Agent:")) != NULL) {
                                k = strstr(line, ":");
                                snprintf(hhd->agent,
                                         MIN(strlen(line), HTTP_PARAM_SIZE - 1),
                                         "%s", k + 2);
                        } else if ((k = strstr(line, "Accept:")) != NULL) {
                                k = strstr(line, ":");
                                strncpy(hhd->accept, k + 2, HTTP_PARAM_SIZE - 1);
                        } else if ((k = strstr(line, "Host:")) != NULL) {
                                k = strstr(line, ":");
                                strncpy(hhd->host, k + 2, HTTP_PARAM_SIZE - 1);
                        } else if ((k = strstr(line, "Connection:")) != NULL) {
                                k = strstr(line, ":");
                                strncpy(hhd->connection, k + 2,
					HTTP_PARAM_SIZE - 1);
                        } else if ((k = strstr(line, "Content-Length:")) != NULL) {
				k = strstr(line, ":");
				strncpy(len, k + 2, 11);
				content_len = atoi(len);
			} else if ((k = strstr(line, "Content-Type:")) != NULL) {
                                k = strstr(line, ":");
                                strncpy(hhd->content_type, k + 2, HTTP_PARAM_SIZE - 1);
                        }
                        
                        memset(line, 0, HTTP_LINE_SIZE);
                        continue;

                } else if (b == 2) {
			b = j = 0;
			t = 1;
			memset(line, 0, HTTP_LINE_SIZE);
			continue;
                }

                if (j < HTTP_LINE_SIZE) {
                        line[j++] = buf[i];
                }

		if (i == (size - 1) &&
		    content_len > 0 &&
		    strcmp(line, "\r\n") != 0 &&
		    strstr(line, ":") == NULL) {
                        if (content_len > HTTP_MAX_POST_SIZE - 1)
                                content_len = HTTP_MAX_POST_SIZE - 1;
                        memset(hhd->post, 0, content_len);
                        strncpy(hhd->post, line, content_len);
                        printf("%s\n", hhd->post);
		}
        }

        return hhd;
	*/
}

static char *http_msg(int code)
{
        switch (code) {
                case 404:
                        return HTTP_MSG_404;
                case 403:
                        return HTTP_MSG_403;
                default:
                        return HTTP_MSG_DEFAULT;
        };
}

int http_header(int fd, int code, char *content_type,
                char *charset, int length)
{
        char time_str[32] = "0";
        time_t t;
        struct tm *tmp;

        if ((t = time(NULL)) != -1 &&
            (tmp = localtime(&t)) != NULL
            && strftime(time_str, sizeof(time_str), "%a, %d %b %Y %H:%M:%S %z", tmp));

        char header[HTTP_HEADER_SIZE];
        char *cstr = "";
        switch (code) {
                case 200:
                        cstr = HTTP_HEADER_200;
                        break;
                case 404:
                        cstr = HTTP_HEADER_404;
                        break;
                case 403:
                        cstr = HTTP_HEADER_403;
                        break;

                default:
                        cstr = HTTP_HEADER_400;
                        break;
        }

        snprintf(header,
                 sizeof(header) - 1,
                 "%s\r\n"
                 "Date: %s\r\n"
                 "Server: %s\r\n"
                 "Content-Length: %d\r\n"
                 "Content-Type: %s\r\n"
                 "Connection: closed\r\n\r\n",
                 cstr, 
                 time_str,
                 TD_NAME,
                 length,
                 content_type);
        send(fd, header, strlen(header), 0);

        return 0;
}

/* 
 * Send error html or msg to client
 */
static int http_method_err(_epoll_data *ed, int code, char *ip)
{
        struct stat sb;
        char filename[1024];
        char *msg;

        http_req_headers *hhd = ed->hhd;

        snprintf(filename, 1023, "%s/%s/%s/err-%d.%s", root_dir, hhd->host,
                 HTTP_ERR_FILE_DIR, code, hhd->ext);

        log2file(hhd->host, hhd->method, hhd->ppath, hhd->version,
                 code, "", 0, hhd->user_agent, ed->ip);

        msg = http_msg(code);
        int msg_size = strlen(msg);
 
        /* Error file is missing? */
        if (stat(filename, &sb) == -1) {
                http_header(ed->fd, code, "text/html",
                            "utf-8", msg_size);
                send(ed->fd, msg, msg_size, 0);
		destroy_event(ed);
                return 0;
        }

        /* Send error file */
        http_header(ed->fd, code, "text/html",
                    "utf-8", sb.st_size);
	destroy_event(ed);

//        net_sendfile(epoll_data);
        return 0;
}

/* Serve http get method */
static int http_method_get(_epoll_data *epoll_data)
{
        char filename[1024];
        char *idx;
        http_req_headers *hhd = (http_req_headers *)epoll_data->hhd;
        char *mime_info;
        char mime[33];
        int code;
        char *buf;

        printf("ok..\n");
	memset(mime, 0, sizeof(mime));

        if ((idx = strchr(hhd->host, ':')) != NULL)
                hhd->host[idx - hhd->host] = '_';

	if (hhd->ext == NULL || strlen(hhd->ext) == 0) {
                snprintf(mime, 32, "text/html");
	} else {
		if (strcmp(hhd->ext, "tdp") == 0) {
                        printf("In Dso: file : %s\n", hhd->file);
                        buf = load_dso(hhd->file, epoll_data);
                        if (buf == NULL) {
                                http_method_err(epoll_data->fd, 403, NULL);
                                return 0;
                        }

                        http_header(epoll_data->fd, 200, "text/html",
                                    "utf-8", strlen(buf));
                        send(epoll_data->fd, buf, strlen(buf), 0);
                        printf("log...\n");
                        printf("%s, %s\n", hhd->host, _http_methods[hhd->method]);
                        log2file(hhd->host, _http_methods[hhd->method],
                                 hhd->ppath, hhd->version,
                                 200, "", strlen(buf),
                                 hhd->user_agent, epoll_data->ip);
                        destroy_event(epoll_data);
                        return 0;
//			return dyn_run(epoll_data);
		}

		mime_info = get_mimetype(_gcon->mime_set, hhd->ext);
		if (mime_info)
			snprintf(mime, 32, mime_info);
	}


        struct stat sb;

        snprintf(filename, 1023, "%s/%s/%s/%s%s%s", root_dir,
                 hhd->host, hhd->path, hhd->file,
                 hhd->ext ? "." : "",
                 hhd->ext ? hhd->ext : "");
        //printf("Get file: %s\n, %s\n", filename, hhd->file);

        if (stat(filename, &sb) != -1) {
                //printf("%d\n", sb.st_size);
                log2file(hhd->host, _http_methods[hhd->method], hhd->ppath, hhd->version,
                         200, "", sb.st_size, hhd->user_agent, epoll_data->ip);
                http_header(epoll_data->fd, 200, mime,
                            strcmp(mime, "text/html") == 0 ? ";charset=utf-8" : "",
                            sb.st_size);

                epoll_data->fsize = sb.st_size;
                epoll_data->ofd = -1;
                strncpy(epoll_data->filename, filename, 128);
                if (net_sendfile(epoll_data) != 0){
			/*
                        fprintf(stderr, "Can not send file: %s\n",
                                filename);
				*/
                        return -1;
                }

                return 0;
        }

        if (strcmp(hhd->file, "index.html") == 0)
                http_method_err(epoll_data, 403, "html");
        else
                http_method_err(epoll_data, 404, "html");

        return 0;
}

int http_proto(_epoll_data *epoll_data)
{
        int err = 0, fd, size;

        http_req_headers *hhd = (http_req_headers *)epoll_data->hhd;

        if (!epoll_data || !hhd) {
                return -1;
        }

        switch (hhd->method) {
                case HTTP_METHOD_GET:
                        printf("get: ok");
                        /* GET method */
                        err = http_method_get(epoll_data);
                        break;

                case HTTP_METHOD_POST:
                        /* POST method */
                        http_header(epoll_data->fd, 200, "text/html", "utf-8",
                                    strlen(hhd->post));
                        if (send(epoll_data->fd, hhd->post,
                                 strlen(hhd->post), 0) <= 0) {
                                err = -1;
                        }
                        break;

                default:
                        /* 400 */
                        size = strlen(HTTP_MSG_400);

                        http_header(epoll_data->fd, 400,
                            "text/html", "utf-8",
                            strlen(HTTP_MSG_400));

                        if (send(epoll_data->fd, HTTP_MSG_400, size, 0) <= 0) {
                                perror("send error\n");
                                err = -1;
                        }
        }

        //free(epoll_data->hhd);

        return (err ? -1 : 0);
}

