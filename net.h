#ifndef NET_INCLUDED
#define NET_INCLUDED

#define PORT                    8888
#define SENDFILE_RETRY_TIMES    10

#include "event.h"

int net_listen(int port);
void setnonblocking(int fd);
int net_sendfile(_epoll_data *ed);

#endif
