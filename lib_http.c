#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <time.h>
#include <errno.h>
#include <ctype.h>

#include "base.h"
#include "event.h"
#include "net.h"
#include "log.h"
#include "config.h"
#include "mime.h"
#include "lib_http.h"
#include "str.h"
#include "hashtab.h"

static char from_hex(char ch)
{
        return isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10;
}

static char to_hex(char code)
{
        static char hex[] = "0123456789abcdef";
        return hex[code & 15];
}

static char *url_decode(char *str)
{
        char *pstr = str, *buf = malloc(strlen(str) + 1), *pbuf = buf;
        while (*pstr) {
                if (*pstr == '%') {
                        if (pstr[1] && pstr[2]) {
                                *pbuf++ = from_hex(pstr[1]) << 4 |
                                        from_hex(pstr[2]);
                                pstr += 2;
                        }
                } else if (*pstr == '+')
                        *pbuf++ = ' ';
                } else
                        *pbuf++ = *pstr;
                pstr++;
        }
        *pbuf = '\0';
        return buf;
}

static int parse_query_string(http_req_headers *hrh)
{
        int size = strlen(hrh->qs);
        int i;

        if (size <= 2)
                return -EINVAL;

	if (!(hrh->get_args = init_elfhashtab(HTTP_MAX_GET_ARG_SIZE))) {
                fprintf("Could not create get args pool\n");
		return -ENOMEM;
        }

        for (i = 0; i < size; i++) {
                switch (hrh->qs[i]) {
                        case HTTP_ARG_SEPARATOR:
                                break;

                        case '=':

			case ' ':
			default:
				continue;
		}
        }

}

static int parse_uri(http_req_headers *hrh)
{
        char *file;
        int i, size, t_size, qs = 0, ext = 0, upath = 0, path = 0;
        int tupath_size, tpath_size, text_size, tfile_size;

        char t[4096], tupath[1024], tpath[1024], text[12], tfile[256];
        t_size 		= sizeof(t);
	tupath_size 	= sizeof(tupath);
	tpath_size 	= sizeof(tpath);
	text_size 	= sizeof(text);
	tfile_size 	= sizeof(tfile);

	int e_ext_size = 0;

        memset(t, 0, size);
	memset(tupath, 0, tupath_size);
	memset(tpath, 0, tpath_size);
	memset(text, 0, text_size);
	memset(tfile, 0, tfile_size);

        strncpy(t, hrh->path, t_size - 1);
        hrh->ppath = mkstr(t, t_size - 1);

        for (i = 0; i < t_size; ++i) {
                if (t[i] == '?') {
                        qs = 1;
                        hrh->qs = mkstr(t + i + 1, HTTP_MAX_QS_SIZE + 1);
                        break;
                }

                if (ext && t[i] != '/' && i < text_size) {
                        text[ext - 1] = t[i];
                        ++ext;
			++e_ext_size;
                        continue;
                }

                if (ext && t[i] == '/') {
                        tupath[0] = '/';
                        upath = 1;
                        ext = 0;
                        continue;
                }

                if (upath && upath < tupath_size) {
                        tupath[upath++] = t[i];
                        continue;
                }

                if (t[i] == '.') {
                        ext = 1;
                        continue;
                }

                // path & host 
                if (i < tpath_size - 1) {
                        tpath[path++] = t[i];
                }
        }

        if (tupath_size > 0)
                hrh->upath = mkstr(tupath, upath + 1);
        if (e_ext_size > 0)
                hrh->ext = mkstr(text, e_ext_size + 1);

        int end = 0, j = 0;

        size = strlen(tpath);
        if (size > 0) {
                if ((file = strrchr(tpath, '/')) == NULL ||
                    (file == &tpath[size - 1])) {
                        end = 1;
                        file = "index";
			hrh->ext = mkstr("html", 5);
                }

                hrh->file = mkstr((end ? file : file + 1),
				  MIN(strlen(file),tfile_size));
                
                if (!end) 
                        for (i = file - tpath + 1; i < size; i++) {
				++j;
                                tpath[i] = '\0';
			}

		hrh->path = mkstr(tpath, size - j + 1);
        }


        printf("QS:%s...EXT:%s...File:%s...Path:%s...Upath:%s\n",
               hrh->qs, hrh->ext, hrh->file, hrh->path, hrh->upath);
        return 0;
}

static int parse_http_option(http_req_headers *hrh,
			     char * word_key, const char *word)
{
	int size_key = strlen(word_key);
	int size_word = strlen(word);

	if (!word_key || !word || size_key < 3 ||
	    strlen(word) < 3 || word_key[size_key - 1] != ':')
		return -EINVAL;

	word_key[size_key - 1] = '\0';

        switch (word_key[0]) {

                case 'A':
			if (STR5CMP((word_key+1), 'c', 'c', 'e', 'p', 't')) {
				if (size_key == 7) {
					hrh->accept = mkstr(word, size_word + 1);
					break;
				}
				if (word_key[7] != '-')
					return -EINVAL;
				if (STR7CMP((word_key+7), 'C', 'h', 'a', 'r', 's', 'e', 't'))
					hrh->accept_charset = mkstr(word, size_word + 1);
				else if (STR8CMP((word_key+7), 'E', 'n', 'c', 'o', 'd', 'i', 'n', 'g') == 0)
					hrh->accept_encoding = mkstr(word, size_word + 1);
				else if (STR8CMP((word_key+7), 'L', 'a', 'n', 'g', 'u', 'a', 'g', 'e'))
					hrh->accept_language = mkstr(word, size_word + 1);
				else if (STR8CMP((word_key+7), 'D' , 'a', 't', 'e', 't', 'i', 'm', 'e'))
					hrh->accept_datetime = mkstr(word, size_word + 1);
				else
					return -EINVAL;
			}

			break;

		case 'C':
			if (strcmp(word_key + 1, "onnection") == 0)
				hrh->connection = mkstr(word, size_word + 1);
			else if (strcmp(word_key + 1, "ontent_length") == 0)
				hrh->content_length = mkstr(word, size_word + 1);
			else if (strcmp(word_key + 1, "ontent_md5") == 0)
				hrh->content_md5 = mkstr(word, size_word + 1);
			else if (strcmp(word_key + 1, "ontent_type") == 0)
				hrh->content_type = mkstr(word, size_word + 1);
			else if (strcmp(word_key + 1, "ookie") == 0)
				hrh->cookie = mkstr(word, size_word + 1);
			else
				return -EINVAL;

		case 'D':
			if (STR3CMP((word_key+1), 'a', 't', 'e'))
				hrh->date = mkstr(word, size_word + 1);
			else
				return -EINVAL;

			break;

		case 'E':
			if (STR5CMP((word_key+1), 'x', 'p', 'e', 'c', 't'))
				hrh->expect = mkstr(word, size_word + 1);
			else
				return -EINVAL;
			break;

		case 'F':
			if (STR3CMP((word_key+1), 'r', 'o', 'm'))
				hrh->from = mkstr(word, size_word + 1);
			else
				return -EINVAL;
			break;

		case 'H':
                        printf("host ok\n");
			if (STR3CMP((word_key+1), 'o', 's', 't'))
				hrh->host = mkstr(word, size_word + 1);
			else
				return -EINVAL;
			break;

		case 'I':
			if (!STR2CMP((word_key+1), 'f', '-'))
                                return -EINVAL;

                        if (strcmp(word_key + 3, "Modified-Since") == 0)
                                hrh->if_modified_since = mkstr(word, size_word + 1);
                        else if (strcmp(word_key + 3, "None-Match") == 0)
                                hrh->if_none_match = mkstr(word, size_word + 1);
                        else if (STR5CMP((word_key+3), 'R', 'a', 'n', 'g', 'e'))
                                hrh->if_range = mkstr(word, size_word + 1);
                        else if (strcmp(word_key + 3, "Unmodified-Since") == 0)
                                hrh->if_unmodified_since = mkstr(word, size_word + 1);
                        else
                                return -EINVAL;
                        break;

                case 'M':
                        if (strcmp(word_key + 1, "ax_Forwards") == 0)
                                hrh->max_forwards = mkstr(word, size_word + 1);
                        else 
                                return -EINVAL;
                        break;

                case 'P':
                        if (STR5CMP((word_key+1), 'r', 'a', 'g', 'm', 'a'))
                                hrh->pragma = mkstr(word, size_word + 1);
                        else if (strcmp(word_key + 1, "roxy-Authorization") == 0)
                                hrh->proxy_authorization = mkstr(word, size_word + 1);
                        else
                                return -EINVAL;
                        break;

                case 'R':
                        if (STR4CMP((word_key+1), 'a', 'n', 'g', 'e'))
                                hrh->range = mkstr(word, size_word + 1);
                        else if (STR6CMP((word_key+1), 'e', 'f', 'e', 'r', 'e', 'r'))
                                hrh->referer = mkstr(word, size_word + 1);
                        else
                                return -EINVAL;
                        break;

                case 'T':
                        if (*(word_key + 1) == 'e')
                                hrh->te = mkstr(word, size_word + 1);
                        else
                                return -EINVAL;
                        break;

                case 'U':
                        printf("U lead: %s, %s\n", word, word_key);
                        if (STR6CMP((word_key+1), 'p', 'g', 'r', 'a', 'd', 'e'))
                                hrh->upgrade = mkstr(word, size_word + 1);
                        else if (strcmp(word_key + 1, "ser-Agent") == 0)
                                hrh->user_agent = mkstr(word, size_word + 1);
                        else
                                return -EINVAL;
                        break;

                case 'V':
                        if (STR2CMP((word_key+1), 'i', 'a'))
                                hrh->via = mkstr(word, size_word + 1);
                        else
                                return -EINVAL;
                        break;

                case 'W':
                        if (STR6CMP((word_key+1), 'a', 'r', 'n', 'i', 'n', 'g'))
                                hrh->warning = mkstr(word, size_word + 1);
                        else
                                return -EINVAL;
                        break;

defaut:
                        return -EINVAL;
        }


        return -EINVAL;
}

static int parse_http_method(http_req_headers *hrh, char *word)
{
	if (word == NULL || (strlen(word) < 3)) {
		fprintf(stderr, "Could not parse http header\n");
		return -EINVAL;
	}

	switch (word[0]) {
		case 'C':
			if (!STR6CMP((word+1), 'O', 'N', 'N', 'E', 'C', 'T'))
				return -EINVAL;
			hrh->method = HTTP_METHOD_CONNECT;
			break;
		case 'D':
			if (!STR4CMP((word+1), 'E', 'L', 'E', 'T'))
				return -EINVAL;
			hrh->method = HTTP_METHOD_DELETE;
			break;
		case 'G':
			if (!STR2CMP((word+1), 'E', 'T'))
				return -EINVAL;
			hrh->method = HTTP_METHOD_GET;
			break;
		case 'H':
			if (!STR3CMP((word+1), 'E', 'A', 'D'))
				return -EINVAL;
			hrh->method = HTTP_METHOD_HEAD;
			break;
		case 'O':
			if (!STR6CMP((word+1), 'P', 'T', 'I', 'O', 'N', 'S'))
				return -EINVAL;
			hrh->method = HTTP_METHOD_OPTIONS;
			break;
		case 'P':
			if (STR2CMP((word+1), 'U', 'T'))
				hrh->method= HTTP_METHOD_POST;
			else if (STR3CMP((word+1), 'O', 'S', 'T'))
				hrh->method= HTTP_METHOD_POST;
			else
				return -EINVAL;
			break;
		case 'T':
			if (!STR4CMP((word+1), 'R', 'A', 'C', 'E'))
				return -EINVAL;
			hrh->method= HTTP_METHOD_TRACE;
			break;

		default:
			return -EINVAL;
	}

	return 0;
}

http_req_headers *http_parse_header(char *buf)
{
        int i, size, j, b, t;
	int ret;
	int content_len = 0;
	char len[12];

	http_req_headers *hrh;

        if ((hrh = malloc(sizeof(*hrh))) == NULL) {
                perror("Can not alloc http header_data\n");
                return NULL;
        }

	hrh->accept = NULL;
	hrh->post = NULL;
	hrh->accept_charset = NULL;
	hrh->accept_encoding = NULL;
	hrh->accept_language = NULL;
	hrh->accept_datetime = NULL;
	hrh->authorization = NULL;
	hrh->cache_control = NULL;
	hrh->connection = NULL;
	hrh->cookie = NULL;
	hrh->content_length = NULL;
	hrh->content_md5 = NULL;
	hrh->date = NULL;
	hrh->expect = NULL;
	hrh->from = NULL; 
	hrh->host = NULL;
	hrh->if_match = NULL;
	hrh->if_modified_since = NULL;
	hrh->if_none_match = NULL;
	hrh->if_range = NULL;
	hrh->if_unmodified_since = NULL;
	hrh->max_forwards = NULL;
	hrh->pragma = NULL;
	hrh->proxy_authorization = NULL;
	hrh->range = NULL;
	hrh->referer = NULL;
	hrh->te = NULL;
	hrh->upgrade = NULL;
	hrh->user_agent = NULL;
	hrh->via = NULL;
	hrh->warning = NULL;
	hrh->method = -1;
        hrh->ext = NULL;
        hrh->ppath = NULL;
        hrh->upath = NULL;
        hrh->qs = NULL;
        hrh->file = NULL;
        hrh->mimetype = NULL;

	char word[256];
	char word_key[256];
	int word_len;

	int last_end 	= 0;
	int line_number = 1;
	int line_len 	= 0;
	int words = 0;
	int tmp = 0;

        size = strlen(buf);
        b = t = j = 0;
        //printf("Origin buf: %s\n", buf);

        for (i = 0; i < size; i++) {
                if (buf[i] > 126 || (buf[i] == '.' && buf[i + 1] == '.'))
                        continue;

//                printf("... char:%c, char+1:%c __ \n", buf[i], buf[i+1]);
                if (i < (size - 1) && i < HTTP_MAX_HEAD_SIZE &&
                    buf[i] == '\r' && buf[i + 1] == '\n') {
                        if (++b == 2)
				t = 1;
		//	printf("Line number: %d\n", line_number);
			continue;
                }

		if (t == 1) {
			if (!(hrh->post = mkstr(&buf[i], size - i + 1)))
				return NULL;
			return 0;
		}

		if (buf[i] == ' ' || buf[i] == '\n') {
                 //       printf("space ");
			memset(word, 0, sizeof(word));

                        if (line_number != HTTP_HEADER_METHOD_LINE && 
                            buf[i] == ' ' && words == 1)
                                continue;

			if (buf[i] == '\n')
				tmp = (i-last_end-1);
			else
				tmp = (i-last_end);


			strncpy(word, (buf + last_end), MIN(255,tmp));
		//	printf("word: %s\n", word);
			last_end = i + 1;
			words++;
                        //printf("words: %d\n", words);
			word_len = strlen(word);

			if (line_number == HTTP_HEADER_METHOD_LINE) {
				if (words == 1 && (ret = parse_http_method(hrh, word)) != 0)
					return ret;
				else if (words == 2) {
					if (word_len == 0) {
						fprintf(stderr, "Invalid http request");
						return -EINVAL;
					}
					hrh->path = mkstr(word, word_len + 1);
				} else if (words == 3) {
					if (word_len != 0)
						hrh->version = mkstr(word, word_len + 1);
                                        parse_uri(hrh);
                                }

                                goto cont;
			}

                      //  printf("g======\n");
			if (words == 1) {
                              //  printf("1: %s\n", word);
				if (word[word_len - 1] != ':') {
                                       // printf("fail\n");
					goto cont;
                                } else {
					memset(word_key, 0, sizeof(word_key));
					strncpy(word_key, word, sizeof(word_key) - 1);
					goto cont;
				}
			} else if (words == 2) {
                             //   printf("word_key:%s, word:%s\n", word_key, word);
				if ((ret = parse_http_option(hrh, word_key, word)) != 0)
					goto cont;
			} else
				goto cont;

		}

cont: 
                if (buf[i] == '\n') {
			++line_number;
                        words = 0;
                }
		b = 0;
		continue;

	}

	return hrh;
}

/*
static int parse_uri(http_req_headers *hrh)
{
        char *file;
        int size, qs = 0, ext = 0, upath = 0, path = 0;

        size = strlen(hrh->path);
        char t[size + 1];

        memset(t, 0, size + 1);
        strncpy(t, hrh->path, size);
        strncpy(hrh->ppath, t, size);

        int i;
        int ext_size = sizeof(hrh->ext),
            upath_size = sizeof(hrh->upath),
            path_size = sizeof(hrh->path),
            file_size = sizeof(hrh->file),
            qs_size = sizeof(hrh->qs);

        memset(hrh->ppath, 0, sizeof(hrh->ppath));
        memset(hrh->path, 0, path_size);
        memset(hrh->upath, 0, upath_size);
        memset(hrh->ext, 0, ext_size);
        memset(hrh->file, 0, file_size);
        memset(hrh->qs, 0, qs_size);

        for (i = 0; i < size; ++i) {
                if (t[i] == '?') {
                        qs = 1;
                        memset(hrh->qs, 0, qs_size);
                        strncpy(hrh->qs, &t[i] + 1, qs_size);
                        break;
                }

                if (ext && ext < ext_size && t[i] != '/') {
                        hrh->ext[ext - 1] = t[i];
                        ++ext;
                        continue;
                }

                if (ext && t[i] == '/') {
                        hrh->upath[0] = '/';
                        upath = 1;
                        ext = 0;
                        continue;
                }

                if (upath && upath < upath_size) {
                        hrh->upath[upath++] = t[i];
                        continue;
                }

                if (t[i] == '.') {
                        ext = 1;
                        continue;
                }

                // path & host
                if (i < path_size - 1) {
                        hrh->path[path++] = t[i];
                }
        }

        int end = 0;
        size = strlen(hrh->path);
        if (size > 0) {
                if ((file = strrchr(hrh->path, '/')) == NULL ||
                    (file == &hrh->path[size - 1])) {
                        end = 1;
                        file = "index";
                        strcpy(hrh->ext, "html");
                }
                strncpy(hrh->file, end ? file : file + 1,
                        MIN(strlen(file),file_size));
                
                if (!end) 
                        for (i = file - hrh->path + 1; i < size; i++)
                                hrh->path[i] = '\0';
        }


        //printf("QS:%s...EXT:%s...File:%s...Path:%s...Upath:%s\n",
         //      hrh->qs, hrh->ext, hrh->file, hrh->path, hrh->upath);
        return 0;
}
*/

