#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "config.h"
#include "mime.h"

global_config *_gcon = NULL;

void init_config()
{
	_gcon = (global_config *)malloc(sizeof(*_gcon));
	if (!_gcon) {
		fprintf(stderr, "Can not allocate global config mem\n");
		exit(-ENOMEM);
	}

	_gcon->mime_set = init_mime_set();
	if (!_gcon->mime_set) {
		fprintf(stderr, "Can not allocate mime mem\n");
		exit(-ENOMEM);
	}

	_gcon->config_set = (config *)malloc(sizeof(config));
	if (!_gcon->config_set) {
		fprintf(stderr, "Can not allocate config mem\n");
		exit(-ENOMEM);
	}

	_gcon->config_set->max_post_size 	= 4096000;
        _gcon->config_set->max_get_size 	= 4096;
        _gcon->config_set->max_sendfile_size	= 10240000;
        _gcon->config_set->max_conn		= 10240;
        _gcon->config_set->max_mem_usage	= 512000000;
        _gcon->config_set->max_cpu_usage	= 0;
}

/* 
 * Parse config file
 */
int parse_config()
{
        FILE *fp;
        char line[256];
        int len;

        if ((fp = fopen(CONFIG_FILE, "r")) == NULL) {
                fprintf(stderr, "Can not open config file: %s\n", CONFIG_FILE);
                exit(-1);
        }

        while (fgets(line, 255, fp)) {
                len = strlen(line);
                if (len <= 2)
                        continue;

                if (line[len - 1] == '\n' || line[len - 1] == '\r') {
                        line[len - 1] = '\0';
                        if (line[len - 2] == '\r' || line[len - 2] == '\n')
                                line[len - 2] = '\0';
                }

                printf("%s\n", line);
        }

        return 0;
}

