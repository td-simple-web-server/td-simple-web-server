#ifndef CONFIG_INCLUDED
#define CONFIG_INCLUDED

//#include "hashtab.h"
#include "mime.h"

/* config struct */
typedef struct {
        long max_post_size;
        long max_get_size;
        long max_sendfile_size;
        long max_conn;
        long max_mem_usage;
        long max_cpu_usage;
} config;

typedef struct {
	config *config_set;
	struct hashtab *mime_set;
} global_config;

#define CONFIG_FILE     "./td.main.cfg"

void            init_config();
int 		parse_config();

#endif
