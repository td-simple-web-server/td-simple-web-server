#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

char *cut_endbr(char *str)
{
        int len;
        
        len = strlen(str);
        if(str[len - 1] == 10)
                str[len - 1] = '\0';
        return str;
}

void strtolower(char str[])
{
	int l = 'a' - 'A';

	for ( ; *str != '\0'; str++)
		if (*str >= 'A' && *str <= 'Z')
			*str = *str + l;
}

char *mkstr(const char *str, size_t size)
{
        if (size <= 0)
                return NULL;

	char *s;

        if ((s = (char *)malloc(size * sizeof(char))) == NULL)
		return NULL;
        bzero(s, size);
        s = strncpy(s, str, size - 1);
//        printf("%s....,%d", s, size);
        return s;
}

char *mkemptystr(int c, size_t size)
{
	if (size <= 0)
		return NULL;

	char *s;

	if ((s = (char *)malloc(size * sizeof(char))) == NULL)
		return NULL;
	memset(s, c, size);
	return s;
}
