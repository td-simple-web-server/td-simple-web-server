#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "dso.h"
#include "hashtab.h"
#include "event.h"
#include "str.h"

//extern char **environ;
extern int errno;

typedef struct {
        void *handle;
        unsigned long count;
        int mtime;
} dso_node;

typedef struct {
        unsigned long count;
        unsigned long cache_hit;
        unsigned long cache_miss;
        struct hashtab *dsoht;
} dso_pool;

dso_pool *global_dso_pool;

int init_dso()
{
        global_dso_pool = malloc(sizeof(*global_dso_pool));
        if (!global_dso_pool) {
                fprintf(stderr, "dso init error\n");
                exit(ENOMEM);
        }

        global_dso_pool->dsoht = init_elfhashtab(DSO_MAX_SIZE);
        if (!global_dso_pool->dsoht) {
                fprintf(stderr, "dsoht init error\n");
                exit(ENOMEM);
        }
        global_dso_pool->count          = 0;
        global_dso_pool->cache_hit      = 0;
        global_dso_pool->cache_miss     = 0;

        return 0;
}

int load_dso(const char *file_name, _epoll_data *ed)
{
        struct stat sb_src, sb_dso;

        char* (*dso_main)(_epoll_data *);
        char *error;
        char dso_cmd[257];
        int size;
        int ret;
        char *pos;
        char exec_main[65];
        char so_path[129];
        char src_path[129];
        dso_node * dn;
	int so_mtime;

        memset(dso_cmd, 0, sizeof(dso_cmd));

        size = strlen(file_name);

        if (!file_name || !size)
                return "";

        snprintf(so_path, 128, "%s/%s.so", DSO_CACHE_DIR, file_name);
        snprintf(src_path, 128, "%s/%s.c", DSO_SRC_DIR, file_name);

        if (stat(src_path, &sb_src) == -1)
                return "";

        snprintf(exec_main, 64, "%s_main", file_name);

        if (!(dn = hashtab_search(global_dso_pool->dsoht, file_name))) {
		if (stat(so_path, &sb_dso) == -1)
			so_mtime = 0;
		else {
			so_mtime = sb_dso.st_mtime;
		}
	} else
		so_mtime = dn->mtime;

	

	if (!dn || so_mtime == 0 ||
	    (sb_src.st_mtime - DSO_EXPIRE_TIME) > so_mtime) {
                snprintf(dso_cmd, 256, "%s %s -o %s %s ./str.c", DSO_COMPILER,
                         DSO_COMPILE_OPT, so_path, src_path);
                printf("dso_cmd: %s\n", dso_cmd);
                if (system(dso_cmd) == -1 && errno != 10) {
                        fprintf(stderr, "dso_cmd exec error: %d\n", errno);
                        return "";
                }

		if (stat(so_path, &sb_dso) == -1) {
			fprintf(stderr, "So create failed\n");
			return "";
		}

		if (!dn) {
			dn = malloc(sizeof(*dn));
			if (!dn) {
				fprintf(stderr, "dso node init error\n");
				return "";
			}

			dn->count = 1;
		} else  {
                        dlclose(dn->handle);
                        dn->handle = NULL;
                }

		dn->mtime = sb_dso.st_mtime;

                dn->handle = dlopen(so_path, RTLD_LAZY);
                if (!dn->handle) {
                        fprintf(stderr, "dlopen error\n");
                        return "";
                }

                global_dso_pool->cache_miss++;

                if (hashtab_insert(global_dso_pool->dsoht,
                                   mkstr(file_name, strlen(file_name) + 1),
                                   dn) != 0)
                        fprintf(stderr, "dso hashtab insert failed\n");
                dlerror();
        } else
		global_dso_pool->cache_hit++;

        global_dso_pool->count++;
	dn->count++;
        printf("count: %d, hit: %d, miss: %d\n", global_dso_pool->count,
               global_dso_pool->cache_hit, global_dso_pool->cache_miss);

        *(void **) (&dso_main) = dlsym(dn->handle, exec_main);
        if ((error = dlerror()) != NULL) {
                fprintf(stderr, "dso exec main error\n");
                return "";
        }

        return (*dso_main)(ed);
}
