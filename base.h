#ifndef BASE_INCLUDED
#define BASE_INCLUDED

#define MIN(a,b)        ((a)<=(b)?(a):(b))
#define MAX(a,b)        ((a)>=(b)?(a):(b))

#define STR2CMP(b, c0, c1)\
	(b[0] == c0 && b[1] == c1)

#define STR3CMP(b, c0, c1, c2)\
	(b[0] == c0 && b[1] == c1 && b[2] == c2)

#define STR4CMP(b, c0, c1, c2, c3)\
	(b[0] == c0 && b[1] == c1 && b[2] == c2 && b[3] == c3)

#define STR5CMP(b, c0, c1, c2, c3, c4)\
	(b[0] == c0 && b[1] == c1 && b[2] == c2 && b[3] == c3 && b[4] == c4)

#define STR6CMP(b, c0, c1, c2, c3, c4, c5)\
	(b[0] == c0 && b[1] == c1 && b[2] == c2 && b[3] == c3 && b[4] == c4 && b[5] == c5)

#define STR7CMP(b, c0, c1, c2, c3, c4, c5, c6)\
	(b[0] == c0 && b[1] == c1 && b[2] == c2 && b[3] == c3 && b[4] == c4 && b[5] == c5 && b[6] == c6)

#define STR8CMP(b, c0, c1, c2, c3, c4, c5, c6, c7)\
	(b[0] == c0 && b[1] == c1 && b[2] == c2 && b[3] == c3 && b[4] == c4 && b[5] == c5 && b[6] == c6 && b[7] == c7)

#define STR9CMP(b, c0, c1, c2, c3, c4, c5, c6, c7, c8)\
	(b[0] == c0 && b[1] == c1 && b[2] == c2 && b[3] == c3 && b[4] == c4 && b[5] == c5 && b[6] == c6 && b[7] == c7 && b[8] == c8)
#endif
