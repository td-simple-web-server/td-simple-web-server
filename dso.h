#ifndef DSO_INCLUDED
#define DSO_INCLUDED

#include "event.h"

#define DSO_COMPILE_OPT " -fPIC -shared "
#define DSO_CACHE_DIR   "./.dso_cache"
#define DSO_COMPILER    "/usr/bin/gcc"
#define DSO_SRC_DIR     "./dso"

#define DSO_MAX_SIZE    4096
#define DSO_EXPIRE_TIME 3

int init_dso();
int load_dso(const char *file_name, _epoll_data *ed);

#endif
