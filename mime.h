#ifndef MIME_INCLUDED
#define MIME_INCLUDED

//#include "lib/uthash.h"
#include "hashtab.h"

#define MIME_TEXT_HTML  "html"
#define MIME_TYPE_FILE  "./mime.types"
#define MIME_SET_SIZE   8192 
#define MIME_KEY_SIZE	128
#define MIME_DATA_SIZE	128

struct hashtab *init_mime_set();
char* get_mimetype(struct hashtab *mime_set, char *key);

#endif
