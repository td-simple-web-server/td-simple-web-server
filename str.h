#ifndef STR_INCLUDED
#define STR_INCLUDED

char *cut_endbr(char *str);
void strtolower(char str[]);
char *mkstr(const char *str, size_t size);


#endif
