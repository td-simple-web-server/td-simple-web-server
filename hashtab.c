/*
 * Author : Stephen Smalley, <sds@epoch.ncsc.mil>
 * modified by debli, <deblicn@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "hashtab.h"

static unsigned int symhash(struct hashtab *h, const void *key)
{
	const char *p, *keyp;
	unsigned int size;
	unsigned int val;

	val = 0;
	keyp = key;
	size = strlen(keyp);
	for (p = keyp; (p - keyp) < size; p++) {
		val = (val << 4 | (val >> (8*sizeof(unsigned int)-4))) ^ (*p);
        }
	return val & (h->size - 1);
}

static unsigned int elfhash(struct hashtab *h, const void *key)
{
        unsigned int hash = 0;  
        unsigned int x    = 0;  
        unsigned int i    = 0;  
        const char   *p   = key;
        unsigned int len  = strlen(p);

        for (i = 0; i < len; p++, i++)  {
                hash = (hash << 4) + (*p);  
                if((x = hash & 0xF0000000L) != 0)  
                        hash ^= (x >> 24);  
                hash &= ~x;  
        }  
        return hash & (h->size - 1);
}

static int symcmp(struct hashtab *h, const void *key1, const void *key2)
{
	const char *keyp1, *keyp2;

	keyp1 = key1;
	keyp2 = key2;
//        printf("%s->%s\n", key1,key2);
	return strcmp(keyp1, keyp2);
}

struct hashtab *hashtab_create(u32 (*hash_value)(struct hashtab *h, const void *key),
			       int (*keycmp)(struct hashtab *h, const void *key1, const void *key2),
			       u32 size)
{
	struct hashtab *p;
	u32 i;

	p = (struct hashtab *)malloc(sizeof(*p));
	if (p == NULL)
		return p;

	p->size = size;
	p->nel = 0;
	p->hash_value = hash_value;
	p->keycmp = keycmp;
	p->htable = malloc(sizeof(*(p->htable)) * size);
	if (p->htable == NULL) {
		free(p);
		return NULL;
	}

	for (i = 0; i < size; i++) {
		p->htable[i] = NULL;
        }

	return p;
}

int hashtab_insert(struct hashtab *h, void *key, void *datum)
{
	unsigned int hvalue = 0;
	struct hashtab_node *prev, *cur, *newnode;

	if (!h || h->nel == HASHTAB_MAX_NODES)
		return -EINVAL;

	hvalue = h->hash_value(h, key);
//        printf("%s => value: %d\n", key, hvalue);
	prev = NULL;
	cur = h->htable[hvalue];
	while (cur && h->keycmp(h, key, cur->key) > 0) {
		prev = cur;
		cur = cur->next;
	}

	if (cur && (h->keycmp(h, key, cur->key) == 0))
		return -EEXIST;

	newnode = malloc(sizeof(*newnode));
	if (newnode == NULL)
		return -ENOMEM;
        memset(newnode, 0, sizeof(*newnode));

	newnode->key = key;
	newnode->datum = datum;
	if (prev) {
		newnode->next = prev->next;
		prev->next = newnode;
	} else {
		newnode->next = h->htable[hvalue];
		h->htable[hvalue] = newnode;
	}

	h->nel++;
	return 0;
}

void *hashtab_search(struct hashtab *h, const void *key)
{
	u32 hvalue;
	struct hashtab_node *cur;

	if (!h)
		return NULL;

	hvalue = h->hash_value(h, key);
	cur = h->htable[hvalue];
	while (cur && h->keycmp(h, key, cur->key) > 0)
		cur = cur->next;

	if (cur == NULL || (h->keycmp(h, key, cur->key) != 0))
		return NULL;

	return cur->datum;
}

void hashtab_destroy(struct hashtab *h)
{
	u32 i;
	struct hashtab_node *cur, *temp;

	if (!h)
		return;

	for (i = 0; i < h->size; i++) {
		cur = h->htable[i];
		while (cur) {
			temp = cur;
			cur = cur->next;
			free(temp);
		}
		h->htable[i] = NULL;
	}

	free(h->htable);
	h->htable = NULL;

	free(h);
}

int hashtab_map(struct hashtab *h,
		int (*apply)(void *k, void *d, void *args),
		void *args)
{
	u32 i;
	int ret;
	struct hashtab_node *cur;

	if (!h)
		return 0;

	for (i = 0; i < h->size; i++) {
		cur = h->htable[i];
		while (cur) {
			ret = apply(cur->key, cur->datum, args);
			if (ret)
				return ret;
			cur = cur->next;
		}
	}
	return 0;
}


void hashtab_stat(struct hashtab *h, struct hashtab_info *info)
{
	u32 i, chain_len, slots_used, max_chain_len;
	struct hashtab_node *cur;

	slots_used = 0;
	max_chain_len = 0;
	for (slots_used = max_chain_len = i = 0; i < h->size; i++) {
		cur = h->htable[i];
		if (cur) {
			slots_used++;
			chain_len = 0;
			while (cur) {
				chain_len++;
				cur = cur->next;
			}

			if (chain_len > max_chain_len)
				max_chain_len = chain_len;
		}
	}

	info->slots_used = slots_used;
	info->max_chain_len = max_chain_len;
}

struct hashtab *init_elfhashtab(unsigned int size)
{
	return hashtab_create(elfhash, symcmp, size);
}

int hashtab_test()
{
        struct hashtab *h;
        int rc;
        char *hs = NULL;
        int i, size;

	struct hashtab_info hi;

        char *keys[] = {
                "k1",
                "k2",
                "k3",
                "k4",
                "k5",
                "k6",
                "k7",
                "k8",
                "k9",
                "k19",
        };

        char *datums[] = {
                "d1",
                "d2",
                "d3",
                "d4",
                "d5",
                "d6",
                "d7",
                "d8",
                "d9",
                "d19",
        };

        h = hashtab_create(elfhash, symcmp, 512);
        if (!h) {
                printf("Hash create error\n");
                exit(-1);
        }

        size = sizeof(keys);
        for (i = 0; i < 10; i++) {
                rc = hashtab_insert(h, keys[i], datums[i]);
                if (rc != 0) {
                        printf("Hash insert error: %d\n", rc);
                        exit(-1);
                }
        }


        for (i = 0; i < 10; i++) {
                hs = hashtab_search(h, keys[i]);
                if (!hs) {
                        printf("Hash `%s` search error\n", keys[i]);
                        exit(-1);
                }

                printf("%s => %s\n", keys[i], hs);
        }


	/*
	if (!(struct hashtab_info *)malloc(sizeof(hi))) {
		printf("No mem for hashtab_info\n");
		exit(-1);
	}
	*/

	hashtab_stat(h, &hi);

	printf("stat\n used: %d, chain_len: %d\n", hi.slots_used, hi.max_chain_len);
	
        return 0;
}
