#ifndef LIB_HTTP_INCLUDED
#define LIB_HTTP_INCLUDED

#include "config.h"
#include "base.h"
#include "hashtab.h"

extern global_config *_gcon;

#define HTTP_HEADER_METHOD_LINE 	1

#define HTTP_METHOD_GET			0
#define HTTP_METHOD_POST		1
#define HTTP_METHOD_OPTIONS		2
#define HTTP_METHOD_HEAD		3
#define HTTP_METHOD_PUT			4
#define	HTTP_METHOD_DELETE		5
#define HTTP_METHOD_TRACE		6
#define	HTTP_METHOD_CONNECT		7

#define HTTP_HEAD_METHOD_LEN		255
#define HTTP_MAX_HEAD_SIZE		MAX(4096,(_gcon->config_set->max_get_size))
#define HTTP_MAX_POST_HEAD_SIZE		MAX(4096,(_gcon->config_set->max_post_size))
#define HTTP_MAX_QS_SIZE                512

#define HTTP_ARG_SEPARATOR              '&'
#define HTTP_MAX_GET_ARG_SIZE		1024

static char *_http_methods[] = {
        "GET",
        "POST",
        "OPTIONS",
        "HEAD",
        "PUT",
        "DELETE",
        "TRACE",
        "CONNCECT",
};

/* http 1.1 request headers */

typedef struct {
	struct hashtab *get_args;
	int method;
	char *post;
	char *version;
	char *path;
	char *accept;
	char *accept_charset;
	char *accept_encoding;
	char *accept_language;
	char *accept_datetime;
	char *authorization;
	char *cache_control;
	char *connection;
	char *cookie;
	char *content_length;
	char *content_md5;
	int content_type;
	char *date;
	char *expect;
	char *from; 
	char *host;
	char *if_match;
	char *if_modified_since;
	char *if_none_match;
	char *if_range;
	char *if_unmodified_since;
	char *max_forwards;
	char *pragma;
	char *proxy_authorization;
	char *range;
	char *referer;
	char *te;
	char *upgrade;
	char *user_agent;
	char *via;
	char *warning;
        char *ppath;
        char *upath;
        char *qs;
        char *file;
        char *ext;
        char *mimetype;
} http_req_headers;

typedef struct {
	char *accept;
	char *accept_charset;
	char *accept_encoding;
	char *accept_language;
	char *accept_datetime;
	char *authorization;
	char *cache_control;
	char *connection;
	char *cookie;
	char *content_length;
	char *content_md5;
	int content_type;
	char *date;
	char *expect;
	char *from; 
	char *host;
	char *if_match;
	char *if_modified_since;
	char *if_none_match;
	char *if_range;
	char *if_unmodified_since;
	char *max_forwards;
	char *pragma;
	char *proxy_authorization;
	char *range;
	char *referer;
	char *te;
	char *upgrade;
	char *user_agent;
	char *via;
	char *warning;
	int method;
} http_resp_headers;

http_req_headers *http_parse_header(char *buf);

#endif
