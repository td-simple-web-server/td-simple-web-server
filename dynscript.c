#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>

#include "event.h"
#include "http.h"

int dyn_run(_epoll_data *ed)
{
        char buf[64] = "Hello, world";

        http_header(ed->fd, 200, "text/html",
                    "utf8", strlen(buf));

        send(ed->fd, buf, strlen(buf), MSG_MORE);

        return 0;
}
